Installing kenlm language model
--------------------------------------
wget -O - http://kheafield.com/code/kenlm.tar.gz |tar xz 

cd kenlm

./bjam -j4

Notes: http://kheafield.com/code/kenlm/

kenlm was chosen becausereports suggested it to have good accuracy and good performance over other language models

Installing kenlm python library
---------------------------------------
pip install https://github.com/kpu/kenlm/archive/master.zip

Training data
--------------------------------------
Since the dev and text data was found to be more related to news and fiction, the below sources were only considered for training

Datasources from nltk: gutenberg,europarl_raw(english),genesis, inaugural,state_union

Steps: 

Install nltk

www.nltk.org/howto/corpus.html

import nltk

nltk.download() 

select options to download the required data

Generating arpa
---------------------------------
bin/lmplz -o 4 < text >text.arpa

(used 4 grams for this assignment)

Generating binary model (will be faster)
---------------------------------
bin/build_binary text.arpa text.binary

Execution steps
------------------------------------
1.	preprocess.py

	To tokenize the training data and replace you are with you're etc. to increase the vocabulary

	Syntax: python preprocess.py -training path/to/input/file > path/to/output/file

	Example: python preprocess.py -training hw3/hw3.dev.err.txt > pdeverror.txt

2.	Generate arpa and binary 
	(using the above steps to generate the language model (4-gram model set -o to 4))

3.	preprocess.py

	To tokenize the testing data 

	Syntax: python preprocess.py -testing path/to/test/file > path/to/output/file

	Example: python preprocess.py -testing hw3/hw3.dev.err.txt > pdeverror.txt

4.	lmclassify.py

	To classify the test data using the language model

	Syntax: python lmclassify.py path/to/preprocessed/file path/to/resulting/file path/to/arpa/binary

	Example: python lmclassify.py pdeverror.txt pdevresult.txt arpabinary.txt

5.	postprocess.py

	Syntax: python postprocess.py path/to/classified/file > path/to/output/file		

Example: python postprocess.py pdevresult.txt > hw3.dev.output.txt

