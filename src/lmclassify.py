import kenlm,sys, re

#model = kenlm.LanguageModel('combineddev.binary')
#file=open('ptest.txt','r')
#out=open('mytest.txt','w')

model = kenlm.LanguageModel(sys.argv[3])
file=open(sys.argv[1],'r')
out=open(sys.argv[2],'w')
res=set()
def main(argv):
	
	global res
	
	for line in file:
		split=False
		score=-2000
		target=''
		res=set()
		#print(line)
		length=len(line)
		if length>1000:
			split=True
		
		if split==False:
			target = checkallpossiblecorrections(line)
		else:
			
			if len(line.split(' . '))>1:
				l = line.split(' . ')
				splitchar = ' . '
				
			elif len(line.split('? ?'))>1:
				l = line.split('? ?')
				splitchar = '? ?'
			for x in l:
				res=set()
				t = checkallpossiblecorrections(x)
				target = target + t + splitchar 
			target = target[:len(target)-len(splitchar)]
			
		out.write(target)

	#print(score)
	file.close()
	out.close()

def checkallpossiblecorrections(line):
	global res
	dual1=[' their '," they're "]
	dual2=[' to ',' too ']
	dual3=[' its '," it's "]
	dual4=[' loose '," lose "]
	dual5=[' your '," you're "]
	score=-2000
	target=''
	res=res.union(set([line]))
	#print(line)
	if " their "  in line:
			#print("their")
		finddual(line,dual1[0],dual1[1])
	if " they're "  in line:
		#print("they're")
		for x in res:
			finddual(x,dual1[1],dual1[0])
	if " to "  in line:
		#print("to")
		for x in res:
			finddual(x,dual2[0],dual2[1])
	if " too "  in line:
		#print("too")
		for x in res:
			finddual(x,dual2[1],dual2[0])
	if " its "  in line:
		#print("its")
		for x in res:
			finddual(x,dual3[0],dual3[1])
	if " it's "  in line:
		#print("it's")
		for x in res:
			finddual(x,dual3[1],dual3[0])
	if " loose "  in line:
		#print("loose")
		for x in res:
			finddual(x,dual4[0],dual4[1])
	if " lose "  in line:
		#print("lose")
		for x in res:
			finddual(x,dual4[1],dual4[0])
	if " your "  in line:
		#print("your")
		for x in res:
			finddual(x,dual5[0],dual5[1])
	if " you're "  in line:
		#print("you're")
		for x in res:
			finddual(x,dual5[1],dual5[0])
	
	for x in res:
		s=model.score(x)
		if s>score:
			score=s
			target=x
	return target
	
def finddual(line, fromstr, tostr):
	l=line.count(fromstr)
	app=''
	i=0
	global res
	#prevconvergecount=len(res)
	while i<l:
		m=re.search(fromstr,line)
		start= app + line[:m.start()] 
		end=line[m.end():]
		#print(start+tostr+end)
		if start+tostr+end not in res:
			#print(start+tostr+end)
			res=res.union(set([start+tostr+end]))
			#print(res)
			finddual(start+tostr+end,fromstr,tostr)
		convergecount=len(res)	
		line=end
		l=line.count(fromstr)
		app=start + fromstr	
		
	#print(res)	

if __name__=="__main__":
	main(sys.argv[1:])
