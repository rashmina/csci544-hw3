import kenlm,sys,re
#Syntax: python postprocess.py mydev.txt > hw3.dev.result.txt
def main(argv):
	file=open(sys.argv[1],'r')
	for line in file:
		s = re.sub(' ([.;,"!?()-]) ', r'\1', line)
		print(s.replace('\n',''))

if __name__=="__main__":
	main(sys.argv[1:])
