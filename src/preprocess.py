import kenlm,sys,re
#Syntax: python preprocess.py -training hw3/hw3.dev.err.txt > pdeverror.txt
def main(argv):
	file=open(sys.argv[2],'r')
	for line in file:
		s = re.sub('([.;,"!?()-])', r' \1 ', line)
		if sys.argv[1]=='-training':
			s=s.replace(' it is '," it's ").replace(' they are '," they're ").replace(' you are '," your're ")
		print(s.replace('\n',''))

if __name__=="__main__":
	main(sys.argv[1:])
